package com.example.intents;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Intent secondIntent = new Intent(this, SecondActivity.class);
        secondIntent.putExtra(Intent.EXTRA_TEXT, "This was from MainActivity");
        startActivity(secondIntent);
    }


    Override
    public boolean onCreateOptionMenu(Menu menu) {
        // Inflate the menu: this adds items to the action bar if is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;



    }

}
